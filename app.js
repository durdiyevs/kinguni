require('dotenv').config();
// Importing Express Module
const express = require("express");
const expressLayout = require("express-ejs-layouts");
// Require Mongoose in order to connect our app
const mongoose = require("mongoose");

const passport = require("passport");
const flash = require("express-flash");
const session = require("express-session");
const methodOverride = require("method-override");
const User = require("./models/User");

// Initialization of our app
const app = express();

const initializePassport = require('./passport-config');
initializePassport(
    passport,
     async(username) => {
         const userFound = await User.findOne({ username })
         return userFound;
     },

     async(id)  => {
        const userFound = await User.findOne({ _id: id })
        return userFound;
     }
) 

// Database connection
mongoose
  .connect("mongodb://localhost/blog", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    // console.log("Connection to mongodb database was successful!")
    // server configurations are here
    app.listen(3000, () =>
      console.log("Server started listening on port:3000")
    );
  }) ;

// middlewares
app.use(express.urlencoded({ extended: true }));
app.use(flash());
app.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
    })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(function(req, res, next) {
  res.locals.isAuthenticated = req.isAuthenticated();
  res.locals.user = req.user;
  next();
});
app.use(methodOverride("_method"));
app.use(express.static("public"));
// app.use("/css", express.static(__dirname + 'public/css'));

app.use(expressLayout);
// To set up our view engine EJS
app.set("view engine", "ejs");

// routes
app.use(require("./routes/index"));
app.use(require("./routes/compose"));
app.use(require("./routes/article"));
app.use(require("./routes/search"));

