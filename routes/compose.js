const router = require("express").Router();
const { checkAuthenticated } = require("../middlewares/auth");
const Blog = require("../models/Blog");

router
  .get("/create", checkAuthenticated, (req, res) => {
    res.render("article/create");
  })

  .post("/create", checkAuthenticated, (req, res) => {
    const { title, description } = req.body;
    const  author = res.locals.user._id;
    console.log(author);
    if (description.length < 20) {
      req.flash(
        "error",
        "Article description should be at least 20 characters long ..."
      );
      res.redirect("/create");
    } else {
      try {
        const blog = new Blog({
          title,
          description,
          author,
        });
        
        blog.save();
        res.redirect("/");
      } catch (error) {
        console.log(error);
        res.redirect("/create");
      }
    }

    // const newBlog = new Blog({ title, description });
    // newBlog
    //   .save()
    //   .then(() => {
    //     console.log("Blog Saved Successfully!");
    //     res.redirect("/");
    //   })
    //   .catch((err) => console.log(err));
  });

module.exports = router;
