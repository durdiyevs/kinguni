const router = require("express").Router();
const Blog = require("../models/Blog");

router.get("/search", (req, res) => {
  try {
    Blog.find(
      {
        $or: [
          { title: { $regex: req.query.search } },
        ],
      },
      (err, data) => {
        if (err) {
          console.log(err);
        } else {
          res.render("../views/pages/search.ejs", { data: data });
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
