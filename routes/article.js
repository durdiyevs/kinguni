const router = require("express").Router();
const { checkAuthenticated } = require("../middlewares/auth");
const Blog = require("../models/Blog");
const User = require("../models/User");

router
  .get("/article/:id", async (req, res) => {
    const { id } = req.params;
    const getArticle = await Blog.findOne({ _id: id });
    res.render("pages/article", { article: getArticle });
  })

  .get("/delete/:id", checkAuthenticated, async (req, res) => {
    const { id } = req.params;
    const user = res.locals.user.username;
    const getArticle = await Blog.findOne({ _id: id });
    const author = await User.findOne({ _id: getArticle.author._id });

    if (user !== author.username) {
      req.flash("error", "You are not allowed to delete this article!");
      res.render("pages/article", { article: getArticle });
    } else {
      Blog.deleteOne({ _id: id })
        .then(() => {
          console.log("Article deleted successfully!");
          res.redirect("/");
        })
        .catch((err) => console.log(err));
    }
  })

  .get("/edit/:id", checkAuthenticated, async (req, res) => {
    const { id } = req.params;
    const getData = await Blog.findOne({ _id: id });
    const user = res.locals.user.username;
    const author = await User.findOne({ _id: getData.author._id });

    if (user !== author.username) {
      req.flash("error", "You are not allowed to edit this article!");
      res.render("pages/article", { article: getData });
    } else {
      res.render("article/edit", { article: getData });
    }
    
  })

  .post("/edit/:id", checkAuthenticated, (req, res) => {
    const { id } = req.params;
    const { title, description } = req.body;

    Blog.updateOne({ _id: id }, { title, description })
      .then(() => {
        console.log("Article updated successfully!");
        res.redirect("/");
      })
      .catch((err) => console.log(err));
  });

module.exports = router;
