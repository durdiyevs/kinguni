const router = require("express").Router();
const passport = require("passport");
const { checkNotAuthenticated } = require("../middlewares/auth");
const User = require("../models/User");
const bcrypt = require("bcryptjs");
const Blog = require("../models/Blog");


router.get("/", async(req, res) => {
    const articles = await Blog.find();
    res.render("pages/home", {articles: articles });
})

router.get("/login", checkNotAuthenticated, (req, res) => {
    res.render("pages/login");
})

router.post("/login", checkNotAuthenticated, passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true,
}))

router.get("/register", checkNotAuthenticated, (req, res) => {
    res.render("pages/register");
})

router.post("/register", checkNotAuthenticated, async (req, res) => {
    const userFound = await User.findOne({username: req.body.username});

    if (userFound){
        req.flash('error', "User with this username already exists!");
        res.redirect('/register');
    } else if(req.body.password !== req.body.confirmation ) {
        req.flash('error', "Passwords do not match!");
        res.redirect('/register');
    } else {
        try {
            const hashedPassword = await bcrypt.hash(req.body.password, 10);
            const user = new User({
                username: req.body.username,
                password: hashedPassword,
            }) 

            await user.save();
            res.redirect('/login');
        } catch (error) {
            console.log(error);
            res.redirect("/register");
        } 
    }
})

router.delete("/logout", (req, res) => {
    req.logout();
    req.session.destroy();
    res.redirect("/login");
})

router.get("/articles", async(req, res) => {
    const articles = await Blog.find();
    res.render("pages/articles", {articles: articles });
})

module.exports = router;
