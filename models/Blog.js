const mongoose = require("mongoose");
const { Schema } = mongoose;

const blogSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true,
    min: 5,
  },

  description: {
    type: String,
    required: true,
    min: 20,
  },

  author: { type: Schema.Types.ObjectId, ref: "User" },

  createdDate: {
    type: Date,
    default: Date.now,
  },
});

const Blog = mongoose.model("Blog", blogSchema);

module.exports = Blog;
