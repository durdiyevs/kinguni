const mongoose = require("mongoose");
const { Schema } = mongoose;

const userShcema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },

  password: {
    type: String,
    required: true,
  },

  articles: [{ type: Schema.Types.ObjectId, ref: "Blog" }]
});

const User = mongoose.model("User", userShcema);

module.exports = User ;
